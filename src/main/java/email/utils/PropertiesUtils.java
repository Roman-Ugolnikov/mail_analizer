package email.utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Logger;

/**
 * Created by Roman on 2/27/2016.
 */
public class PropertiesUtils {

    private static Properties prop = new Properties();
    private static InputStream input = null;

    public static Logger logger = Logger.getLogger(PropertiesUtils.class.getName());


    private PropertiesUtils() {
    }

    public static final String CONFIG_PROPERTIES_FILE_NAME = "config.properties";

    static{
        logger.info("start loading properties ... ");
        try {
            input = new FileInputStream(CONFIG_PROPERTIES_FILE_NAME);
            logger.warning(CONFIG_PROPERTIES_FILE_NAME + " bites: " + input.available());
            prop.load(input);

            logger.info("Loaded " + prop.size() + " values.");
        } catch (IOException e) {
            logger.warning("Can not load properties, Error: " + e.getStackTrace());
        }
    }

    public String getProperty(String key){
        return prop.getProperty(key);
    }

    public String getProperty(String key, String defaultValue){
        return prop.getProperty(key, defaultValue);
    }

    public static Properties getProperties() {
        return prop;
    }

}
