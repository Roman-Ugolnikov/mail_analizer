package email;


import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import org.apache.commons.io.IOUtils;

import javax.swing.*;
import java.awt.print.PrinterException;
import java.io.FileInputStream;
import java.io.IOException;

/**
 * @author Roman Uholnikov
 */
public class Main extends Application {

    @FXML
    private Button printButton;

    @FXML
    private Button doIt;

    @FXML
    private Label hintLabel;

    @FXML
    private TextArea settings;

    @FXML
    private TextArea wordsTranslatedArea;

    @FXML
    private ProgressIndicator progressBar;

    @Override
    public void start(Stage primaryStage) throws Exception {
        FXMLLoader loader = new FXMLLoader(
                getClass().getResource("sample.fxml")
        );
        loader.setController(this);
        Parent root = (Parent) loader.load();
        primaryStage.setTitle("Mail analyzer.");
        primaryStage.setScene(new Scene(root, 800, 600));


        printButton.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                print(wordsTranslatedArea.getText());
            }
        });

        doIt.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                getTranslationConcurrently(false);
            }
        });

        try {
            settings.setText(IOUtils.toString(new FileInputStream("config.properties")));
        } catch (IOException e) {
            e.printStackTrace();
        }

        primaryStage.getIcons().add(new Image("https://upload.wikimedia.org/wikipedia/commons/4/4f/Googlemaillogo.png"));
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }


    public void getTranslationConcurrently(Boolean doTranslate) {
        progressBar.progressProperty().unbind();
        wordsTranslatedArea.textProperty().unbind();
        hintLabel.textProperty().unbind();
        if (MailProcessor.task != null && MailProcessor.task.isRunning()) {
            MailProcessor.task.cancel(true);
            progressBar.setProgress(0);
            wordsTranslatedArea.clear();
            hintLabel.setText("");
        }

        MailProcessor.task = new MailProcessor(settings);
        progressBar.progressProperty().bind(MailProcessor.task.progressProperty());
        wordsTranslatedArea.textProperty().bind(MailProcessor.task.valueProperty());
        hintLabel.textProperty().bind(MailProcessor.task.messageProperty());


        Thread thread = new Thread(MailProcessor.task);
        thread.setDaemon(true);
        thread.start();
    }

    /**
     * @param str The string  to be printed.
     */
    public void print(String str) {

        JTextPane textPane = new JTextPane();

        textPane.setText(str);

        try {
            textPane.print();
        } catch (PrinterException e) {
            //e.printStackTrace();
            System.out.println("problem with printing " + e.getMessage());
        }
    }

}
