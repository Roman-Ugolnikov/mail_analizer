package email;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.logging.Logger;

import javax.mail.BodyPart;
import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.internet.MimeMultipart;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;

import javafx.concurrent.Task;
import javafx.scene.control.TextArea;


/**
 * @author Roman Uholnikov
 */
public class MailProcessor extends Task<String> {

    public static Logger logger = Logger.getLogger(MailProcessor.class.getName());

    public static final SimpleDateFormat SIMPLE_DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");
    public static final SimpleDateFormat SIMPLE_TIME_FORMAT = new SimpleDateFormat("HH:mm:ss");
    public static Task task;
    private TextArea textArea;

    public MailProcessor(TextArea textArea) {
        this.textArea = textArea;
    }

    @Override
    public String call() {
        StringBuilder stringBuilder = new StringBuilder("");
        updateProgress(0, 1);
        updateMessage("Starting");

        stringBuilder.append(readAndTransformMessages());

        updateProgress(1, 1);
        updateValue(stringBuilder.toString());
        updateMessage("");
        return stringBuilder.toString();
    }

    private String readAndTransformMessages() {
        StringBuilder resultSQL = new StringBuilder();
        Folder folder = null;
        Store store = null;
        try {
            Properties props = new Properties(); //PropertiesUtils.getProperties();
            props.load(new ByteArrayInputStream(textArea.getText().getBytes()));
            int minMessagesToBeProceed = Integer.valueOf(props.getProperty("mail.min.messages.to.be.read"));

            Session session = Session.getDefaultInstance(props, null);
            // session.setDebug(true);
            store = session.getStore("imaps");
            updateMessage("Trying to connect: " + props.getProperty("mail.credentials.mail"));
            store.connect(props.getProperty("mail.imap.host"), props.getProperty("mail.credentials.mail"), props.getProperty("mail.credentials.password"));
            folder = store.getFolder("Inbox");
      /* Others GMail folders :
       * [Gmail]/All Mail   This folder contains all of your Gmail messages.
       * [Gmail]/Drafts     Your drafts.
       * [Gmail]/Sent Mail  Messages you sent to other people.
       * [Gmail]/Spam       Messages marked as spam.
       * [Gmail]/Starred    Starred messages.
       * [Gmail]/Trash      Messages deleted from Gmail.
       */
            folder.open(Folder.READ_WRITE);
            int messageCount = folder.getMessageCount();
            int messageToBeReadCount = folder.getUnreadMessageCount() < minMessagesToBeProceed ? minMessagesToBeProceed : folder.getUnreadMessageCount();
            resultSQL.append("\n" + "No of Messages : " + messageCount);
            resultSQL.append("\n" + "No of Unread Messages : " + folder.getUnreadMessageCount());
            resultSQL.append("\n" + "No of Messages to be read : " + messageToBeReadCount);
            resultSQL.append("\n");
            logger.info(MessageFormat.format("Getting messages {0} - {1}", messageCount - messageToBeReadCount, messageCount));
            Message messages[] = folder.getMessages(messageCount - messageToBeReadCount, messageCount);
            for (int i = 0; i < messages.length; ++i) {

                updateProgress(i, messages.length);
                updateValue(resultSQL.toString());
                updateMessage("");

                logger.info("Getting message #" + i);
                Message msg = messages[i];

                logger.info("Got message #" + i);
                if (!msg.getFlags().contains(Flags.Flag.DELETED)) {
                    final String sql = proceedMessage(msg, Boolean.valueOf(props.getProperty("mail.remove.after.processing")));
                    logger.info("message #" + i + "has been read");
                    if(sql != null && !StringUtils.contains(resultSQL, sql)) {
                        resultSQL.append("\n" + sql);
                    }
                }
            }
        } catch (MessagingException ex) {
            resultSQL.append("error:" + ex.getMessage());
        } catch (IOException e) {
            resultSQL.append("error:" + e.getMessage());
        } finally {
            if (folder != null) {
                try {
                    folder.close(true);
                } catch (MessagingException e) {
                    //e.printStackTrace();
                }
            }
            if (store != null) {
                try {
                    store.close();
                } catch (MessagingException e) {
                    e.printStackTrace();
                }
            }
        }
        return resultSQL.toString();
    }

    private String proceedMessage(Message msg, boolean removeMsg) throws IOException, MessagingException {

        logger.info("proceeding message");
        final String subject = msg.getSubject().toLowerCase();
        logger.info("message Subject:" + subject);
        logger.info("detecting table to be inserted");
        final String sql;
        switch (subject) {
            case "weight":
                sql = generateInsertWeight(msg);
                removeMessageIfNeeded(msg, removeMsg, subject);
                return sql;
            case "sleep":
                sql = generateInsertSleep(msg);
                removeMessageIfNeeded(msg, removeMsg, subject);
                return sql;
            case "food":
                sql = generateInsertFood(msg);
                removeMessageIfNeeded(msg, removeMsg, subject);
                return sql;
            case "training":
                sql = generateInsertTraining(msg);
                removeMessageIfNeeded(msg, removeMsg, subject);
                return sql;
        }

        return null;
    }

    private void removeMessageIfNeeded(final Message msg, final boolean removeMsg, final String subject) throws MessagingException {
        if (removeMsg) {
            logger.info("removing " + subject);
            msg.setFlag(Flags.Flag.DELETED, true);
            logger.info("removed" + subject);
        }
    }


    private String generateInsertTraining(Message msg) throws IOException, MessagingException {
        String messageBody = getMessageBody(msg);
        String comment = parseComments(messageBody);
        Date date = parseDate(msg, 1);
        Date time = parseTime(msg, 1);
        String subject = parseSubjects(messageBody);


        String sql = MessageFormat.format("INSERT INTO \"training\" (\"comment\", \"date\", \"time\", \"subject\")  " +
                        "VALUES(''{0}'', ''{1}'', ''{2}'', ''{3}'');",comment, SIMPLE_DATE_FORMAT.format(date), SIMPLE_TIME_FORMAT.format(time),
                subject);
        return sql;

    }

    private String generateInsertFood(Message msg) throws IOException, MessagingException {
        String messageBody = getMessageBody(msg);
        String comment = parseComments(messageBody);
        Date date = parseDate(msg, 1);
        Date time = parseTime(msg, 1);
        Integer duration = parseDuration(messageBody);


        String sql = MessageFormat.format("INSERT INTO \"food\" (\"comment\", \"date\", \"time\", \"duration\")  " +
                        "VALUES(''{0}'', ''{1}'', ''{2}'', ''{3}'');",comment, SIMPLE_DATE_FORMAT.format(date), SIMPLE_TIME_FORMAT.format(time),
                duration.toString());
        return sql;

    }

    private String generateInsertSleep(Message msg) throws IOException, MessagingException {
        logger.info("reading body");
        String messageBody = getMessageBody(msg);
        logger.info("reading comment");
        String comment = parseComments(messageBody);

        logger.info("reading date");
        Date endDate = parseDate(msg);

        //minus one day
        Date startDate = new Date(endDate.getTime() - 24 * 3600 * 1000l);

        //mandatory fields.
        logger.info("reading time 1");
        Date startTime = parseTime(msg, 1);
        logger.info("reading time 2");
        Date endTime = parseTime(msg, 2);

        String sql = MessageFormat.format("INSERT INTO \"sleep\" (\"comment\", \"start_date\", \"start_time\", \"end_date\", \"end_time\")  " +
                "VALUES(''{0}'', ''{1}'', ''{2}'', ''{3}'', ''{4}'');",comment, SIMPLE_DATE_FORMAT.format(startDate), SIMPLE_TIME_FORMAT.format(startTime),
                SIMPLE_DATE_FORMAT.format(endDate), SIMPLE_TIME_FORMAT.format(endTime));
        return sql;

    }

    private String getMessageBody(final Message msg) throws IOException, MessagingException {
        logger.info("reading body");
        Object content = msg.getContent();
        logger.info("body has bean read");
        if (content instanceof MimeMultipart){
            logger.info("body is type of MimeMultipart");
            MimeMultipart mimeMultipartContent = (MimeMultipart) content;
            if (mimeMultipartContent.getCount() > 0) {
                final BodyPart bodyPart = mimeMultipartContent.getBodyPart(0);
                logger.info("body type is " + bodyPart.getContentType());
                return bodyPart.getContent().toString();
            }
        }
        if (content instanceof String) {
            logger.info("body is type of String");
            return StringEscapeUtils.unescapeHtml4(content.toString());
        }
        logger.info("body is NOT type of MimeMultipart or STRING, it is " + content.getClass().getName());
        return ((MimeMultipart) msg.getContent()).getBodyPart(0).getContent().toString().trim();
    }

    private String generateInsertWeight(Message msg) throws IOException, MessagingException {
        logger.info("reading body");
        String messageBody = getMessageBody(msg);
        String weightString = parseWeight(messageBody);
        Date date = parseDate(msg);
        String comment = parseComments(messageBody);
        Date dateForTime = parseTime(msg);

        String sql = "INSERT INTO \"weight\" (\"weight\", \"date\", \"time\", \"comment\") " +
                "VALUES('" + weightString + "', '" + SIMPLE_DATE_FORMAT.format(date) + "', '" + SIMPLE_TIME_FORMAT.format(dateForTime) + "', '" + comment + "');";
        return sql;
        //INSERT INTO "weight" ("weight", "date", "time", "comment") VALUES('67.5', '2001-02-17', '14:40:00', 'some comment')
    }

    private Integer parseDuration(String messageBody) {
        Integer result = 5;
        List<String> lines = Arrays.asList(StringUtils.split(messageBody, "\n"));
        for (String line : lines) {
            try {
                result = Integer.valueOf(StringUtils.substringBefore(line, "min").trim());
                return result;
            }catch (NumberFormatException e){
                //not a weight
            }
        }
        //default value
        return result;
    }

    private String parseWeight(String messageBody) {
        double result = 0.0;
        List<String> lines = Arrays.asList(StringUtils.split(messageBody, "\n"));
        for (String line : lines) {
            try {
                result = Double.valueOf(line.trim().replace(",", "."));
                return line.trim().replace(",", ".");
            }catch (NumberFormatException e){
                //not a weight
            }
        }
        return "0.0";
    }

    private String parse(String word, String messageBody) {
        StringBuilder result = new StringBuilder("");
        List<String> lines = Arrays.asList(StringUtils.split(messageBody, "\n"));
        String commentPrefix = word.toLowerCase();
        lines.stream()
                .filter(line -> line.toLowerCase().startsWith(commentPrefix))
                .forEach(line -> result.append(StringUtils.substringAfter(line.toLowerCase(), commentPrefix)));

        return result.toString();
    }

    private String parseComments(String messageBody) {
        return parse("comment", messageBody);
    }

    private String parseSubjects(String messageBody) {
        return parse("subject", messageBody);
    }


    private Date parseDate(Message msg) throws IOException, MessagingException {
        return parseDate(msg, 1);
    }

    /**
     * get Date.
     *
     * @param msg
     * @param index index of found date. If message has several dates, you whould specify which day you need. start form
     *              1
     * @return
     * @throws IOException
     * @throws MessagingException
     */
    private Date parseDate(Message msg, int index) throws IOException, MessagingException {
        Date result = msg.getSentDate();
        String messageBody = getMessageBody(msg);
        List<String> lines = Arrays.asList(StringUtils.split(messageBody, "\n"));
        int foundIndex = 0;
        for (int i = 0; i < lines.size(); i++) {
            if (lines.get(i).contains("/")) {
                try {
                    result.setDate(Integer.valueOf(StringUtils.substringBetween(lines.get(i), " ", "/")));
                    try {
                        result.setDate(Integer.valueOf(StringUtils.substringAfter(lines.get(i), "/")));
                    } catch (NumberFormatException ex) {
                        result.setDate(Integer.valueOf(StringUtils.substringBetween(lines.get(i), "/", "/")));
                        result.setYear(Integer.valueOf(StringUtils.substringAfterLast(lines.get(i), "/").trim()));
                    }
                    foundIndex++;
                    if (foundIndex == index) {
                        return result;
                    }
                } catch (NumberFormatException e) {
                    //is not a number
                }
            }
        }
        return result;
    }

    private Date parseTime(Message msg) throws IOException, MessagingException {
        return  parseTime(msg, 1);
    }

    private Date parseTime(Message msg, int index) throws IOException, MessagingException {
        Date result = msg.getSentDate();
        String messageBody = getMessageBody(msg);
        List<String> lines = Arrays.asList(StringUtils.split(messageBody, "\n"));
        int foundIndex = 0;
        for (int i = 0; i < lines.size(); i++) {
            if (lines.get(i).contains(":")) {
                try {
                    result.setHours(Integer.valueOf(StringUtils.substringBefore(lines.get(i), ":")));
                    result.setMinutes(Integer.valueOf(StringUtils.substringAfter(lines.get(i), ":").trim()));

                    foundIndex++;
                    if (foundIndex == index) {
                        return result;
                    }
                } catch (NumberFormatException e) {
                    //is not a number
                }
            }
        }
        return result;
    }

}
